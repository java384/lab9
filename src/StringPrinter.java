public class StringPrinter {

    public StringPrinter(String thingToPrint){
        this.thingToPrint = thingToPrint;
    }

    void print(){
        System.out.println(thingToPrint);
    }

    String thingToPrint;
}
