public class IntegerPrinter {

    public IntegerPrinter(int thingToPrint){
        this.thingToPrint = thingToPrint;
    }

    void print(){
        System.out.println(thingToPrint);
    }

    int thingToPrint;
}
