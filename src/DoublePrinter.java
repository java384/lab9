public class DoublePrinter {

    public DoublePrinter(double thingToPrint){
        this.thingToPrint = thingToPrint;
    }

    void print(){
        System.out.println(thingToPrint);
    }

    double thingToPrint;
}
